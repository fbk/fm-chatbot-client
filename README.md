# Chat widget for Rasa #

This chat widget can be used to deploy virtual assistants made with [Rasa](https://rasa.com/) on any website.

## Features ##

* Pure JavaScript component
* Works with Rasa's REST channel
* Easy setup
* Text Messages
* Supports Markdown formatting
* Customizable with CSS and JS
* Generates a unique session id
* Images and buttons are not supported yet

## Usage ##

The widget is designed to be used with Rasa and uses
the [REST channel](https://rasa.com/docs/rasa/connectors/your-own-website/#rest-channels).

Embed the ```fm-chatbot.js``` in the HTML of your website and configure it to connect to your Rasa bot either use
the [jsdelivr](https://www.jsdelivr.com/) hosted version.

```html

<!DOCTYPE html>
<html lang="en">
<head>
    <title>A chatbot demo page...</title>
    <script src="https://cdn.jsdelivr.net/npm/@claudio.giuliano/fm-chatbot-client@1" type="text/javascript"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>
<div id="fm-chatbot"></div>

<script lang="JavaScript">

    new FmChatbot().create({
        selector: "fm-chatbot", // the id of the div container
        initPayload: "/start", // intial payload
        customData: {"language": "en"}, // arbitrary custom data
        title: "I'm a chatbot", // chat title
        apiUrl: "http://localhost:5050/webhooks/rest/webhook",  // the rest endpoint 
        subtitle: "Ask me anything!",  // chat subtitle
        tooltip: "Hi I'am a chatbot, click to open the chat", // the tooltip to display for the open chat button
        closeTooltip: "Click to close the chat", // the tooltop to display fort the close chat button        
        hideWhenServiceNotAvailable: false, // hide the chat when the server is not available
        serviceNotAvailableMessage: "Service not available at the moment.", // message to display when the service is not available (if hideWhenServiceNotAvailable: true)
        inputTextFieldHint: "Ask a question...", // a short hint that describes the expected input
        open: true, // open the chat when the document is loaded
        customMessageDelay: 500, // delay before the bot answer is showed
        // aria-label messages for open, close, send buttons and input
        openAriaLabel: "Open the chat",
        closeAriaLabel: "Close the chat",
        sendAriaLabel: "Send the message",
        inputAriaLabel: "Input message",
        // functions to call when the chat is opened and closed
        onWidgetEvent: {
            "onChatOpen": () => console.log('the chat component is opening...'),
            "onChatClose": () => console.log("the chat component is closing...")
        },
        // function to call when the a bot message is received
        onApiEvent: {
            'bot_uttered': () => console.log('the chatbot said something')
        }
    });

</script>
</html>
```

In your Rasa bot setup, make sure to include the Rasa REST channel in your ```credentials.yml``` file:

```yaml
rest:
# pass
```

Restart your Rasa server. Depending on your setup you might need to add CORS headers, e.g. ```--cors "*"```.

```shell
rasa run --credentials ./credentials.yml  --enable-api --model ./models --endpoints ./endpoints.yml --cors "*"

```

## Similar projects ##

* https://github.com/scalableminds/chatroom
* https://github.com/botfront/rasa-webchat

## Production

The source is hosted on [jsdelivr](https://www.jsdelivr.com/).

```shell
// load any project hosted on npm

https://cdn.jsdelivr.net/npm/@claudio.giuliano/fm-chatbot-client@1.0.0/file


// load @claudio.giuliano/fm-chatbot-client v1.0.0

https://cdn.jsdelivr.net/npm/@claudio.giuliano/fm-chatbot-client@1.0.0/fm-chatbot.js


// use a version range instead of a specific version

https://cdn.jsdelivr.net/npm/@claudio.giuliano/fm-chatbot-client@1.0/fm-chatbot.js

https://cdn.jsdelivr.net/npm/@claudio.giuliano/fm-chatbot-client@2/fm-chatbot.js


// omit the version completely to get the latest one

// you should NOT use this in production

https://cdn.jsdelivr.net/npm/@claudio.giuliano/fm-chatbot-client/fm-chatbot.min.js


// add ".min" to any JS/CSS file to get a minified version

// if one doesn't exist, we'll generate it for you

https://cdn.jsdelivr.net/npm/@claudio.giuliano/fm-chatbot-client@1.0.0/fm-chatbot.min.js


// omit the file path to get the default file

https://cdn.jsdelivr.net/npm/@claudio.giuliano/fm-chatbot-client@1.0.0


// add / at the end to get a directory listing

https://cdn.jsdelivr.net/npm/@claudio.giuliano/fm-chatbot-client@1.0.0/
```